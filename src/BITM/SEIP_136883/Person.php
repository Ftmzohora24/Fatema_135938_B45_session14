<?php

namespace App;


class Person
{

    private $Name;
    private $Dob;

    public function setName($Name)
    {
        $this->Name = $Name;
    }
    public function getName()
    {
        return $this->Name;
    }

    public function setDob($Dob)
    {
        $this->Dob = $Dob;
    }

    public function getDob()
    {
        return $this->Dob;
    }

}